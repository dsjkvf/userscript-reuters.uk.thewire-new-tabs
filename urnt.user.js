// ==UserScript==
// @name        reuters.uk - open links in new tabs
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reuters.uk.thewire-new-tabs
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reuters.uk.thewire-new-tabs/raw/master/urnt.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reuters.uk.thewire-new-tabs/raw/master/urnt.user.js
// @match       *://uk.reuters.com/theWire*
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @run-at      document-end
// @grant       none
// @version     1.0.2
// ==/UserScript==

function linksInNewTabs() {
    var links = document.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        if (links[i].href.indexOf("article") != -1) {
            links[i].target = '_blank';
        };
    };
};
waitForKeyElements("h3.article-heading", linksInNewTabs );
